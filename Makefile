PYTHON_VERSION := $(shell poetry run python -c 'import platform;print(platform.python_version())')

.PHONY: all docker docker-up
.DEFAULT: all

all:
	poetry build

poetry.lock: pyproject.toml
	poetry lock

requirements.txt: poetry.lock
	poetry export -f $@

docker: docker/Dockerfile
	docker build \
		--file $< \
		--build-arg PYTHON_VERSION=${PYTHON_VERSION} \
		--tag 'psab:latest' \
		${DOCKER_OPTS} \
		.

docker-up: docker/docker-compose.yml
	cd docker && \
	docker-compose -f docker-compose.yml up
