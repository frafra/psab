from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from pycsw.server import Csw

import configparser

#name = 'pycsw'
#user = settings.DATABASES['default']['USER']
#password = settings.DATABASES['default']['PASSWORD']
#host = settings.DATABASES['default']['HOST']

pycsw_config = configparser.SafeConfigParser()
pycsw_config.read('/srv/psab/catalog/default.cfg')
#pycsw_config['repository']['database'] = f'postgresql://{user}:{password}@{host}/{name}'

@csrf_exempt
def csw_wrapper(request):
    """CSW wrapper"""

    # initialize pycsw
    # pycsw_config: either a ConfigParser object or a dict of
    # the pycsw configuration
    #
    # env: dict of (HTTP) environment (defaults to os.environ)
    #
    # version: defaults to '3.0.0'
    my_csw = Csw(pycsw_config, request.environ, version='2.0.2')

    # dispatch the request
    http_status, response = my_csw.dispatch_wsgi()
    http_status_code = int(http_status.split()[0])

    return HttpResponse(response, status=http_status_code, content_type=my_csw.contenttype)
