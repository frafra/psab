#!/bin/sh -ex

export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

app="$1"
domain="$2"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR/../.."

source venv/bin/activate
cd apps
wq start -d "$app.$domain" "$app"

