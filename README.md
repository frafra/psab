# Installation

## Vagrant (preferred)

About Vagrant: https://www.vagrantup.com/

```
$ vagrant up
```

Service will be available on http://localhost:8080.

## Centos 7

```
# bash install-centos7.sh
```

Service will be available on http://localhost.

## Use a different domain

1. Change site domain in Django admin interface (password is `changeme`)
2. Change `Servername` in `/etc/httpd/conf.d/psab.conf`
3. Reload httpd: `# systemctl reload httpd`

# Limitations

- PyCSW is not working properly
- Application creation is a synchronous operation
- SELinux is not supported

