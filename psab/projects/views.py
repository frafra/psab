from django.contrib.auth.decorators import login_required
from django.db.models import Prefetch
from django.shortcuts import render, redirect
from django import forms
from django.contrib.sites.models import Site

from .models import *
from .forms import ProjectForm

def list_projects(request):
    current_site = Site.objects.get_current()
    domain = current_site.domain
    projects = Project.objects.order_by('identifier').select_related('owner').prefetch_related(Prefetch('tags', to_attr='tags_list'))
    return render(request, 'list.html', {
        "projects":projects,
        "domain":domain,
    })

@login_required(login_url='/accounts/login/')
def new_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST, request.FILES)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            # http://django-taggit.readthedocs.io/en/stable/forms.html#commit-false
            form.save_m2m()
            return redirect('/')
    else:
        form = ProjectForm()
    return render(request, 'new.html', {'form': form})

