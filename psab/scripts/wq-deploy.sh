#!/bin/sh -ex

export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

app="$1"
form="$2"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR/../.."

unset DJANGO_SETTINGS_MODULE
source venv/bin/activate
cd "apps/$app/db"
cp "$app/settings/prod.py" "$app/settings/dev.py"
./manage.py migrate --no-input
./manage.py collectstatic --no-input
yes | wq addform "$form"
cd ..
yes n | ./deploy.sh 0.0.1
cd htdocs
mkdir -p assets/www
cp -r * assets/www || true
cp -rn ../../../psab/template/* .

zip -r "$app" *
cp -r "$app.zip" "$app.apk"
java -jar /srv/venv/bin/uber-apk-signer.jar --overwrite -a "$app.apk"
mv "$app.apk" ../../../psab/static/apps/"$app.apk"

touch ../../httpd-reload
