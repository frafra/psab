#!/bin/sh

cd "$( dirname "${BASH_SOURCE[0]}" )"

while [ $? == 0 ]
do
    inotifywait -e attrib ../../apps/httpd-reload &&
        sleep 1 &&
        systemctl reload httpd
done
