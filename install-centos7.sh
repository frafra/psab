#!/bin/bash

set -ex

mv --no-target-directory /home/vagrant/psab /srv
chown -R root:root /srv

yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum install -y httpd mailcap postgresql-server postgis nodejs zip inotify-tools python36u python36u-mod_wsgi java-1.8.0-openjdk-headless
yum install -y gcc libxml2-devel libxslt-devel httpd-devel postgresql-devel python36u-devel python36u-pip

postgresql-setup initdb

cat << EOF > /var/lib/pgsql/data/pg_hba.conf
local   all             all                                     peer
host    all             all             127.0.0.1/32            md5
host    all             all             ::1/128                 md5
EOF

systemctl enable postgresql
systemctl start postgresql

cat << EOF | su - postgres -c psql
CREATE USER admin WITH PASSWORD 'changeme';
ALTER USER admin SUPERUSER;
CREATE DATABASE psab OWNER admin;
CREATE DATABASE pycsw OWNER admin;
EOF

cat <<EOF | su - postgres -c psql pycsw
CREATE EXTENSION postgis;
EOF

cd /srv
python3.6 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
curl -sL https://github.com/patrickfav/uber-apk-signer/releases/download/v1.0.0/uber-apk-signer-1.0.0.jar -o venv/bin/uber-apk-signer.jar

export SERVERNAME="localhost"
envsubst < /srv/psab/conf/psab.conf.example > /srv/psab/conf/psab.conf
ln -s /srv/psab/conf/psab.conf /etc/httpd/conf.d/
ln /srv/conf/psab.service /etc/systemd/system/
chown -R apache /srv/{apps,psab/{media,static}}
pycsw-admin.py -c setup_db -f psab/catalog/default.cfg


cd /srv/psab
./manage.py migrate --no-input
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@localhost', 'changeme')" | ./manage.py shell

cat << EOF >> /etc/httpd/conf/httpd.conf
IncludeOptional /srv/apps/*/conf/*.conf
EOF

cat << EOF > /etc/selinux/config
SELINUX=permissive
SELINUXTYPE=targeted
EOF
setenforce 0

systemctl enable httpd
systemctl start httpd

systemctl enable psab
systemctl start psab
