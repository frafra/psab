from django.conf.urls import url
from . import views

urlpatterns = [
    url('^new/', views.new_project),
    url('^', views.list_projects),
]
