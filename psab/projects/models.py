from django.db import models
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
from django.template.loader import render_to_string
from django.core.validators import RegexValidator
from django.contrib.sites.models import Site

from taggit.managers import TaggableManager

import fileinput
import os
import pathlib
import psycopg2
import secrets
import shutil
import string
import subprocess
import urllib.request

def path_and_rename(instance, filename):
    new_filename = f'uploads/{instance.identifier}.xlsx'
    destination = os.path.join(settings.MEDIA_ROOT, new_filename)
    if os.path.isfile(destination):
        os.unlink(destination)
    return new_filename

alphanumericlower = RegexValidator(r'^[0-9a-z]*$',
    'Only alphanumeric lowercase characters are allowed.')

class Project(models.Model):
    title = models.CharField(max_length=50)
    identifier = models.CharField(max_length=50, unique=True, validators=[alphanumericlower])
    description = models.TextField()
    tags = TaggableManager()
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    creation = models.DateTimeField(default=timezone.now, blank=True)
    upload = models.FileField(upload_to=path_and_rename)

    def __str__(self):
        return self.identifier

@receiver(post_save, sender=Project)
def project_saved(sender, instance, created, **kwargs):
    current_site = Site.objects.get_current()
    domain = current_site.domain
    if not created:
        return
    name = settings.DATABASES['default']['NAME']
    user = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    with psycopg2.connect(dbname=name, user=user, password=password, host=host) as conn:
        conn.set_isolation_level(0)
        with conn.cursor() as cursor:
            appname = instance.identifier
            appuser = appname
            apppassword = ''
            for i in range(16):
                apppassword += secrets.choice(string.ascii_letters)
            cursor.execute("CREATE USER {appuser} WITH PASSWORD %s".format(**vars()), [apppassword])
            cursor.execute("CREATE DATABASE {appname} OWNER {appuser}".format(**vars()))
        newconn = psycopg2.connect(dbname=appname, user=user, password=password, host=host)
    with newconn:
        newconn.set_isolation_level(0)
        with newconn.cursor() as cursor:
            cursor.execute("CREATE EXTENSION postgis")
    
    app_path = os.path.join(settings.BASE_DIR, '..', 'apps', appname)
    xlsx_path = os.path.join(settings.MEDIA_ROOT, instance.upload.name)

    subprocess.run([os.path.join(settings.BASE_DIR, 'scripts', 'wq-start.sh'), appname, domain], check=True, stdout=subprocess.PIPE)

    for line in fileinput.input(os.path.join(app_path, "conf", f"{appname}.conf"), inplace=True):
        if "${APACHE_LOG_DIR}" not in line:
            print(line.replace(f"apps/{appname}/venv", "venv"))
    settings_path = os.path.join(app_path, "db", appname, "settings", "prod.py")
    for line in fileinput.input(settings_path, inplace=True):
        if "'PASSWORD': ''" in line:
            print(line.replace("''", f"'{apppassword}'"))
        elif "'USER': ''" in line:
            print(line.replace("''", f"'{appuser}'"))
        elif "'HOST': ''" in line:
            print(line.replace("''", f"'{host}'"))
        else:
            print(line)
    with open(settings_path, 'a') as settings_file:
        settings_file.write("\nALLOWED_HOSTS = ['*']\n")

    subprocess.run([os.path.join(settings.BASE_DIR, 'scripts', 'wq-deploy.sh'), appname, xlsx_path], check=True, stdout=subprocess.PIPE)

    with newconn:
        with newconn.cursor() as cursor:
            cursor.execute("""INSERT INTO auth_user
                (id, password, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", [
                1,
                instance.owner.password,
                True,
                instance.owner.username,
                instance.owner.first_name,
                instance.owner.last_name,
                instance.owner.email,
                True,
                instance.owner.is_active,
                instance.owner.date_joined,
            ])

    rendered = render_to_string('insert.xml', {
        "title":instance.title,
        "description":instance.description,
        "creator":f"{instance.owner.username} {instance.owner.email}",
        "tags":[tag.name for tag in instance.tags.all()],
        "created":instance.creation,
        "identifier":instance.identifier,
        "publisher":"PSAB",
        "uri":f"http://{instance.identifier}.{domain}/{instance.identifier}s.geojson",
        "license":"https://opendatacommons.org/licenses/odbl/1.0/",
    })
    request = urllib.request.Request(f'http://{domain}/csw?', data=rendered.encode('utf-8'), method='POST')
    with urllib.request.urlopen(request) as f:
        pass #print(f.read().decode('utf-8'))

    control = os.path.join(settings.BASE_DIR, '..', 'apps', 'httpd-reload')
    pathlib.Path(control).touch()

@receiver(post_delete, sender=Project)
def project_deleted(sender, instance, *args, **kwargs):
    name = settings.DATABASES['default']['NAME']
    user = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    host = settings.DATABASES['default']['HOST']
    with psycopg2.connect(dbname=name, user=user, password=password, host=host) as conn:
        conn.set_isolation_level(0)
        with conn.cursor() as cursor:
            appname = instance.identifier
            appuser = appname
            cursor.execute(f"DROP DATABASE IF EXISTS {appname}")
            cursor.execute(f"DROP USER IF EXISTS {appuser}")

    app_path = os.path.join(settings.BASE_DIR, '..', 'apps', appname)
    shutil.rmtree(app_path)

    control = os.path.join(settings.BASE_DIR, '..', 'apps', 'httpd-reload')
    pathlib.Path(control).touch()

